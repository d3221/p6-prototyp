var config = {
  "mqtt_ip": "192.168.137.15",
  "wobbleFactor": 30,
  "wordCountAlert": 10,
  "minTalkTime": 20,
  "minEmotionTrigger": 0.2,
  "projectionOffset": "+21"
};
