// Parameters
var hostname = config.mqtt_ip;
var port = 1884;


// Create a client instance
var client = new Paho.MQTT.Client(hostname, Number(port), "clientId");

// Set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;

// Connect the client
client.connect({
    onSuccess: onConnect
});

function onConnect() {
    console.log("DEV: onConnect");
    client.subscribe("#");
}

function onConnectionLost(responseObject) {
    if (responseObject.errorCode !== 0) {
        console.log("DEV: onConnectionLost:" + responseObject.errorMessage);
    }
}

function onMessageArrived(message) {
    var topic = message.destinationName;

    if (topic != "reset") {
      var topic = topic.split("/");
      var role = topic[0];
      var type = topic[1];
      var payload = message.payloadString;

      writeAction(type, payload, role);
      systemAction();
    } else {
      console.log("we have a reset");

      // Reset person JSON
      person = personDefault;

      // Reset started
      started = false;

      // Reset lights
      lightStarts();
      lightOff(0);
      lightOff(1);
      lightOff(2);
      lightOff(3);
    }
}
