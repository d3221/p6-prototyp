function getRoleById(id) {
  var trigPerson;
  $.each(person, function (key, data) {
      if (person[key].seat == id) {
        trigPerson = key;
        return false;
      }
  });
  return trigPerson;
}


function getIdByRole(role) {
  return person[role].seat;
}


// Add talktime to a role
function addTime(role, time) {
  var currentTime = person[role].talktime;
  var newTime = currentTime+parseFloat(time);
  person[role].talktime = newTime;
}


// Calculate the whole talk time
function getWholeTalktime() {
    var wholeTime = 0.0;
    $.each(person, function (key, data) {
      wholeTime += data.talktime;
    });
    return Math.ceil(wholeTime);
}


// Calculate the percent of the role for talking
function getPercentTalktime(role) {
  var wholeTime = getWholeTalktime();
  var percent = person[role].talktime/wholeTime;
  return percent;
}


// Get the person where the light is shining on
function getActivePerson() {
  var activePerson = "";
  jQuery.each(person, function(i, val) {
    if (val.lightFired !== false) {
      activePerson = i;
      return false;
    }
  });
  return activePerson;
}


// Check if there is an active block variable
function checkActiveBlock() {
  var block = false;
  jQuery.each(person, function(i, val) {
    if (val.block !== false) {
      block = true;
      return false;
    }
  });
  return block;
}


// Check if all seats are taken
function checkAllSeats(){
  var allSeats = true;
  jQuery.each(person, function(i, val) {
    if(person[i].seat == 99){
      allSeats = false;
      return false;
    }
  });
  return allSeats;
}
