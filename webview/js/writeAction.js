function writeAction(type, payload, role) {
  //console.log("DEV: Type: " + type);
  //console.log("DEV: Rolle: " + role);
  //console.log("DEV: Payload: " + payload);


  // Write seat position
  if (type == "pos") {
    person[role].seat = parseInt(payload);
    lightOn(payload);
  }


  // Write mimes
  if (type == "mime") {
    // Write the mime to a person
    payload = payload.split("/");
    person[role].mime[payload[0]] = parseFloat(payload[1]);
  }


  // Write insultTrigger
  if (type == "insult") {
    payload = payload.split(";");
    id = payload[0];
    trigPerson = getRoleById(id);
    person[trigPerson].insultTrigger = true;
  }


  // Write wordTrigger
  if (type == "trigger" && role == "speech") {
    payload = payload.split(";");
    id = payload[0];
    trigPerson = getRoleById(id);
    person[trigPerson].wordTrigger = true;
  }


  // Calculate new talktime
  if (type == "time") {


    // Split the payload up
    payload = payload.split(";");
    id = payload[0];
    spokenTime = payload[1];

    // (ONLY COUNT THE TIME UP IF IT'S NOT THE TRIGGERED PERSON!!!!)
    if (getActivePerson() != getRoleById(id)) {
      addTime(getRoleById(id), spokenTime);
    }

    // Do we have a triggered person which is talking so we can reset the light?
    if (getActivePerson() !== "") {
      if (getActivePerson() == getRoleById(id)) {
        console.log("We have a person to activate: " + getRoleById(id));
        moveLight(id, true);
        person[getRoleById(id)].lightFired = false;
        person[getRoleById(id)].block = true;
      }
    }
  }
}
