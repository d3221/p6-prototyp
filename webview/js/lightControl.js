////////////////////////////////////////////////////////////////////////////
//////////////////////// INITIALIZE THE LIGHTSYSTEM ////////////////////////
////////////////////////////////////////////////////////////////////////////
var lightDiameter = $(".light").height();
var windowHeight = $(window).height();
var windowWidth = $(window).width();

var positionMiddleHorizontal = (windowWidth/2) - (lightDiameter/2);
var positionMiddleVertical = (windowHeight/2) - (lightDiameter/2) + parseInt(config.projectionOffset);
var leftRightOffset = (windowWidth-windowHeight)/2;



function lightStarts() {
  $("#0").css({
    marginLeft: positionMiddleHorizontal + "px",
    marginTop: config.wobbleFactor + "px"
  });

  $("#1").css({
    marginTop: positionMiddleVertical + "px",
    marginLeft: leftRightOffset + "px"
  });

  $("#2").css({
    marginLeft: (windowWidth - lightDiameter - leftRightOffset) + "px",
    marginTop: positionMiddleVertical + "px"
  });

  $("#3").css({
    marginTop: (windowHeight - lightDiameter - config.wobbleFactor) + "px",
    marginLeft: positionMiddleHorizontal + "px"
  });
}
lightStarts();

// Animate the light bulbs
function animation() {
  $(".light").each(function() {
    var randomX = Math.floor((Math.random() * config.wobbleFactor) + (-config.wobbleFactor));
    var randomY = Math.floor((Math.random() * config.wobbleFactor) + (-config.wobbleFactor));

    $(this).css({
      transform:"translate("+randomX+"px, "+randomY+"px)"
    });
  });
}
var animationInterval = setInterval(animation, 1050);
setTimeout(animationInterval, 3000);


////////////////////////////////////////////////////////////////////////////
///////////////////// HERE THE FUNCTIONS ARE FOLLOWING /////////////////////
////////////////////////////////////////////////////////////////////////////

// Turns on a light
function lightOn(id) {
  $("#"+ id).css("opacity", "1");
}

// Turn off a light
function lightOff(id) {
  $("#"+ id).css("opacity", "0");
}


// Lightpoints sum up in the middle when all 4 seats are taken
function centerAllLights(){
  if (checkAllSeats(person) === true){
    jQuery.each(person, function(i) {
      $("#" + person[i].seat).css({
        marginLeft: positionMiddleHorizontal + "px",
        marginTop: positionMiddleVertical + "px"
      });
    });
  }
}


// Is there already an active light?
function checkActiveLight() {
  alreadyLight = false;
  jQuery.each(person, function(i, val) {
    if (val.lightFired !== false) {
      alreadyLight = true;
      return false;
    }
  });
  return alreadyLight;
}


// Move the light to position
function moveLight(id, backToCenter = false) {
  if ((!checkActiveLight()) || backToCenter) {
    if (id == 0) {
      direction = "top";
      if (backToCenter) {
        addition = "+";
      } else {
        addition = "-";
      }
    }

    if (id == 1) {
      direction = "left";
      if (backToCenter) {
        addition = "+";
      } else {
        addition = "-";
      }
    }

    if (id == 2) {
      direction = "left";
      if (backToCenter) {
        addition = "-";
      } else {
        addition = "+";
      }
    }

    if (id == 3) {
      direction = "top";
      if (backToCenter) {
        addition = "-";
      } else {
        addition = "+";
      }
    }


    console.log("Move light for #" + id);
    $("#"+id).css("margin-" + direction, addition + "=" + (positionMiddleVertical-config.wobbleFactor) + "px");
    person[getRoleById(id)]["lightFired"] = true;

  } else {
    console.log("There is already an active light!");
  }
}
