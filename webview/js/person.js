// Person with specifications
var person = {
  "shaper": {
    "lightFired": false,
    "block": false,
    "talktime" : 0,
    "seat" : 99,
    "wordTrigger": false,
    "insultTrigger": false,
    "mime":{
      "sadness": 0,
      "anger": 0
    },
    "talkPercentage": {
      "tooLess": 0.20,
      "tooMuch": 0.33
    },
    "counterparts": {
      "insultTrigger":"teamworker",
      "wordTrigger":"teamworker",
      "sadness":"teamworker",
      "anger":"teamworker",
      "talkTime": {
        "tooLess":"resource",
        "tooMuch": "plant"
      }
    }
  },


  "plant":{
    "lightFired": false,
    "block": false,
    "talktime" : 0,
    "seat" : 99,
    "wordTrigger": false,
    "insultTrigger": false,
    "mime":{
      "sadness": 0,
      "anger": 0
    },
    "talkPercentage": {
      "tooLess": 0.20,
      "tooMuch": 0.30
    },
    "counterparts": {
      "insultTrigger":"resource",
      "wordTrigger":"resource",
      "sadness":"teamworker",
      "anger":"resource",
      "talkTime": {
        "tooLess":"teamworker",
        "tooMuch": "shaper"
      }
    }
  },


  "teamworker":{
    "lightFired": false,
    "block": false,
    "talktime" : 0,
    "seat" : 99,
    "wordTrigger": false,
    "insultTrigger": false,
    "mime":{
      "sadness": 0,
      "anger": 0
    },
    "talkPercentage": {
      "tooLess": 0.15,
      "tooMuch": 0.20
    },
    "counterparts": {
      "insultTrigger":"shaper",
      "wordTrigger":"shaper",
      "sadness":"resource",
      "anger":"resource",
      "talkTime": {
        "tooLess":"resource",
        "tooMuch": "plant"
      }
    }
  },


  "resource":{
    "lightFired": false,
    "block": false,
    "talktime" : 0,
    "seat" : 99,
    "wordTrigger": false,
    "insultTrigger": false,
    "mime":{
      "sadness": 0,
      "anger": 0
    },
    "talkPercentage": {
      "tooLess": 0.20,
      "tooMuch": 0.30
    },
    "counterparts": {
      "insultTrigger":"shaper",
      "wordTrigger":"shaper",
      "sadness":"teamworker",
      "anger":"plant",
      "talkTime": {
        "tooLess":"teamworker",
        "tooMuch": "plant"
      }
    }
  }
};





/* HOLD THE DEFAULT STATE FOR RESET */
var personDefault = person;
