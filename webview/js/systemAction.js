// Is the scenario already started and light are on and centered?
var started = false; // DEBUG: FALSE ON DEFAULT!!!!!! ONLY SET TO TRUE TO TEST

// Now look through every person and act like predicted!
function systemAction() {
  // center all lights the first time every seat is taken
  if (!started && checkAllSeats()) {
    $('.light').css("transition-delay", "2s");
    centerAllLights();
    $('.light').css("transition-delay", "0s");
    started = true;
  } else {
      // Loop through every role and do decisions
      jQuery.each(person, function(role, info) {
        // Is a person blocking the normal routine?
        if (!checkActiveBlock()) {
          ///////////////////////////
          // Do we have an insult? //
          ///////////////////////////
          if (info.insultTrigger) {
            console.log(role + ": we have an insult!");

            // Move the light to the counterpart
            moveLight(getIdByRole(person[role].counterparts.insultTrigger));
            person[role].insultTrigger = false;
          }


          //////////////////////////////////////////////////////
          // Do we have too much words in the wordCountAlert? //
          //////////////////////////////////////////////////////
          if (info.wordTrigger) {
            console.log(role + ": we have too much trigger words!");

            // Move the light to the counterpart
            moveLight(getIdByRole(person[role].counterparts.wordTrigger));
            person[role].wordTrigger = false;
          }


          /////////////////////////////////
          // What does the emotions say? //
          /////////////////////////////////
          mimes = person[role].mime; // shortcut
          var sadnessCounterpart, angerCounterpart;

          if (mimes.sadness > config.minEmotionTrigger) {
            sadnessCounterpart = person[role].counterparts.sadness;
          }

          if (mimes.anger > config.minEmotionTrigger) {
            angerCounterpart = person[role].counterparts.anger;
          }


          if (sadnessCounterpart !== undefined && angerCounterpart !== undefined) {
            //console.log("DEV: activate sadnessCounterpart");
            moveLight(getIdByRole(sadnessCounterpart));
          } else {
            if (sadnessCounterpart) {
              moveLight(getIdByRole(sadnessCounterpart));
              //console.log("DEV: activate sadnessCounterpart");
            }

            if (angerCounterpart) {
              moveLight(getIdByRole(angerCounterpart));
              //console.log("DEV: activate angerCounterpart");
            }
          }


          ////////////////////////////////////////
          // Do we have a much- or less-talker? //
          ////////////////////////////////////////
          if (getWholeTalktime() >= config.minTalkTime) {
            //console.log("they are talking over X seconds");

            // Define when and who is the envolved counterpart
            var lessCounterpart, muchCounterpart, tooLess, tooMuch;
            tooMuch = person[role].talkPercentage.tooMuch;
            tooLess = person[role].talkPercentage.tooLess;
            lessCounterpart = person[role].counterparts.talkTime.tooLess;
            muchCounterpart = person[role].counterparts.talkTime.tooMuch;

            var ownPercent = getPercentTalktime(role);
            //console.log("DEV: " + role + ">> " + ownPercent);

            if (ownPercent > tooMuch) {
              moveLight(getIdByRole(muchCounterpart));
              //console.log("DEV: Activate: (much) "+ muchCounterpart);
            }

            if (ownPercent < tooLess) {
              moveLight(getIdByRole(lessCounterpart));
              //console.log("DEV: Activate: (less) "+ lessCounterpart);
            }
          }
        } else {
          // so now give the person free for the next routine
          person[role].block = false;
        }
    });
  }
}
