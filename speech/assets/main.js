/////////////////////////////////////////
//////////// CHANGE IP HERE /////////////
/////////////////////////////////////////
var mqtt_ip = "192.168.137.15";
/////////////////////////////////////////

/////////////////////////////////////////
///// HOW MANY TRIGGERWORDS AT ONCE /////
var maxCountWords = 10;
/////////////////////////////////////////




var raspberry_ip = mqtt_ip;
var person = {
  "shaper": {
    "position": 99,
    "words":[
      "jetzt",
      "sofort",
      "lasst uns",
      "das dauert zu lange",
      "müssen",
      "konkreter",
      "ja aber"
    ],
    "count":0
  },
  "plant": {
    "position": 99,
    "words":[
      "wie wäre es",
      "versuchen wir doch",
      "und wenn wir",
      "ich habe da eine idee",
      "ich habe eine idee",
      "jetzt mal ganz anders"
    ],
    "count":0
  },
  "resource": {
    "position": 99,
    "words":[
      "ich hab da letztens was gesehen",
      "ich kenne da jemanden",
      "kontakte"
    ],
    "count":0
  },
  "teamworker": {
    "position": 99,
    "words":[
      "ich weiß nicht",
      "unsicher",
      "ich könnte das nicht",
      "jemand anderes",
      "gut gemacht",
      "weiter so",
      "danke"
    ],
    "count":0
  }
};




var client;
client = mows.createClient("ws://" + raspberry_ip + ":1884/mqtt");

client.subscribe('shaper/pos');
client.subscribe('plant/pos');
client.subscribe('resource/pos');
client.subscribe('teamworker/pos');
client.subscribe('reset');


client.on('message', function (topic, message) {
  if (topic != "reset") {
    role = topic.split("/")[0];
    person[role]["position"] = parseInt(message);
  } else {
    console.log("we have a reset");
    reset();
  }
});


// Publish to MQTT server
function mqtt_publish(topic, payload) {
  client && client.publish(topic, payload);
}


// Reset the system
function reset() {
  // Reset seat position
  person.shaper.position = 99;
  person.plant.position = 99;
  person.resource.position = 99;
  person.teamworker.position = 99;

  // Reset word count
  person.shaper.count = 99;
  person.plant.count = 99;
  person.resource.count = 99;
  person.teamworker.count = 99;
}


// Check if everyone is there
function checkAllSeats() {
  if (
    person["shaper"]["position"] != 99 &&
    person["plant"]["position"] != 99 &&
    person["resource"]["position"] != 99 &&
    person["teamworker"]["position"] != 99
  ) {
    return true;
  } else {
    return false;
  }
}


// Is there a triggerword?
function triggerCheck(id, text) {
  // Get the target person
  target = 99;
  jQuery.each(person, function(role, info) {
    if(person[role]["position"] == id) {
      target = role;
    }
  });

  // Now analyze the text for triggerwords
  triggerlist = person[target]["words"];
  text = text.toLowerCase();
  countThis = 0;
  jQuery.each(triggerlist, function(i, word) {
    var regExp = new RegExp(word, "gi");
    count = (text.match(regExp) || []).length;
    countThis += count;
  });

  // Now add the "found words count" to the general person count
  person[target]["count"] += countThis;

  // And now we check if these are more than they should!
  if (person[target]["count"] > maxCountWords) {
    person[target]["count"] = 0;
    return true;
  } else {
    return false;
  }

}


// Is there an insult?
function insultCheck(text) {
  insultText = text+"*"; // sick bugfixing... otherwise the match function kills the JS because of 0 matches....
  matches = insultText.match(/\*/gi).length;
  if (matches > 1) {
    insult = true;
  } else {
    insult = false;
  }
  return insult;
}


// Calculate the speech time
function speechTime(offset, duration) {
  secondDevider = 10000;
  //return ((duration-offset)/secondDevider)/1000;
  return duration/10000/100/10;
}

// Process the analyzed data from the sound
function processData(data, role) {
  if (checkAllSeats()) {
    console.log("DEV: Everyone is there");
    displayText = data.DisplayText;
    insult = insultCheck(displayText);
    secondsOfSpeech = speechTime(data.Offset, data.Duration);

    console.log("DEV: Zeit des Texts: " + secondsOfSpeech+"s");
    console.log("DEV: Beleidigung: " + insult);

    // Insult check
    if (insult) {
      mqtt_publish("speech/insult", role + ";" + insult.toString());
    }

    // Triggerword check
    if (triggerCheck(role, displayText)) {
      console.log("too many");
      mqtt_publish("speech/trigger", role + ";true");
    }

    mqtt_publish("speech/time", role + ";" + secondsOfSpeech.toString());
  } else {
    console.log("DEV: Not anyone is there.");
  }
}
