import time
from colorDetection import colors
from roleManagement import *
from face import *

# Global needed vars
allRoles = False
roles = []


try:
	while True:
		# Are all four roles there?
		if not allRoles:
			roles = getRoles()
			print("DEV: In Main:")
			print("DEV: " + str(roles))
			print("Check Roles:")
			if checkRoles(roles):
				allRoles = True
			print("DEV: -------")

			# Publish the role positions to MQTT for the Web Canvas
			postRoles(roles, "pos")


		else:
			print("DEV: All four roles are available! Let's go further.")

			# Publish the role positions to MQTT for the Web Canvas
			postRoles(roles, "pos")

			# Now analyze their faces
			checkEmotions = ["sadness", "anger"]
			emotions = face("emotion", checkEmotions)
			postEmotions(roles, emotions, checkEmotions)

			# Wait 10 seconds for a change
			time.sleep(10)

except KeyboardInterrupt:
    print('interrupted!')
