import os
import time
from colorDetection import colors

# Translate the fetched color to a rolename
def getRoleByColor(color):
	if color == "red":
		return "shaper"

	elif color == "green":
		return "plant"

	elif color == "yellow":
		return "teamworker"

	elif color == "blue":
		return "resource"

	else:
		return "none"


# Get the roles sitting positions
def getRoles():
	print("-----------------")
	print ("Hey; is everyone there?")

	roles = []

	missing = False
        # Are there all four colors?
        for color in colors():

	        if color == 'none':
        		missing = True
		#else:
		roles.append(getRoleByColor(color))


	if not missing:
        	print("All roles are available")
	else:
        	print("Roles are missing...")

                # Wait a short time
                time.sleep(5)
                print("Let's check this again!")

	return roles


# Check if every role is available
def checkRoles(roles):
	for role in roles:
		missing = False
		if role == "none":
			missing = True
			break

	if missing:
		return False
	else:
		return True


# Post the role sitting positions to MQTT
def postRoles(roles, type):
	for idx, role in enumerate(roles):
		if role != "none":
			os.system("mosquitto_pub -h localhost -t " + role + "/" + type + " -m \"" + str(idx) + "\"")
