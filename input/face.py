import os
import json
from threading import Thread
from keys import *

# Set a default imagecollection name
camcollection = "camcollection.jpg"


# Thread function: capture an image for #? cam
def captureImage(deviceId):
	os.system("sudo fswebcam -d /dev/video" + str(deviceId) + " --no-banner -r 400x255 -S 5 cam" + str(deviceId) + ".jpg 2>/dev/null")


# Thread function: remove an image for #?
def removeImage(imageId):
	os.system("rm -rf cam" + str(imageId) + ".jpg")


# Merge all four camera images into one big
def imageMerge():
        import numpy as np
        from PIL import Image

	# Capture all cams with threading
	captureThreads = []
	for idx, i in enumerate(range(1, 5)):
		captureThreads.append(Thread(target=captureImage, args=(i,)))
		captureThreads[idx].start()

	# Wait until every thred is done
	for idx, thread in enumerate(captureThreads):
		captureThreads[idx].join()

        # Merging process
	images_list = ['cam1.jpg', 'cam2.jpg', 'cam3.jpg', 'cam4.jpg']
        imgs = [ Image.open(i) for i in images_list ]
        min_img_shape = sorted( [(np.sum(i.size), i.size ) for i in imgs])[0][1]
        img_merge = np.hstack( (np.asarray( i.resize(min_img_shape,Image.ANTIALIAS) ) for i in imgs ) )
        img_merge = Image.fromarray( img_merge)
        img_merge.save(camcollection)

	# Remove all single images after merging with threading
	removeThreads = []
        for idx, i in enumerate(range(1, 5)):
                removeThreads.append(Thread(target=removeImage, args=(i,)))
                removeThreads[idx].start()

        # Wait until every thred is done
        for idx, thread in enumerate(removeThreads):
                removeThreads[idx].join()

# Main function for getting the emotions
def face(attribute, neededEmotions):
	import urllib
	import requests


	# Create the photo strip with all cams
	imageMerge()


	# Read the camera collection image
	with open(camcollection, 'rb') as f:
	    	img_data = f.read()


	# Fetch the needed sub. key
	subscription_key = key("face")


	# Set request headers
	header = {
	    'Content-Type': 'application/octet-stream',
	    'Ocp-Apim-Subscription-Key': subscription_key,
	}


	# Set request parameters
	params = urllib.urlencode({
	    'returnFaceId': 'true',
	    'returnFaceLandmarks': 'false',
	    'returnFaceAttributes': 'smile,emotion',
	})

	api_url = "https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect?%s"%params


	# Perform the request ...
	r = requests.post(api_url,
                  headers=header,
                  data=img_data)

	# ... and fetch the data in a JSON
	data = r.json()


	# Left-order workaround
	all = [[],[],[],[]]

	order = []
	for idx, position in enumerate(data):
		left = position["faceRectangle"]["left"]
		order.append(left)
	order.sort()


	# Set the output as needed for a further processing
	for idx, face in enumerate(data):
		output = []
		faceAttr = data[idx]["faceAttributes"]
		left = data[idx]["faceRectangle"]["left"]
		smile = faceAttr["smile"]
		emotions = faceAttr[attribute]

		# Loop through the needed emotions
		for emotion in neededEmotions:
			output.append(emotions[emotion])

		# Loop through the order and match the positions to the faces
		for arrayPos, position in enumerate(order):
			if order[arrayPos] == left:
				all[arrayPos].append(output)
				break


		#all.append(output)

	#print(all)
	os.system("rm -rf " + camcollection)
	return all


#################
# EXAMPLE USAGE #
#################

#face = face("emotion", ["happiness", "sadness"])

# 1. WHAT you want (string) | "emotion"
# 2. Which things from it (array) | ["happiness", "sadness"]


#face("emotion", ["happiness", "sadness"])


# Function to post the emotions to the MQTT broker
def postEmotions(roles, emotions, emotionNames):
	# Loop through the roles
        for idx, role in enumerate(roles):
		# Change IDX to emotion ID
                if idx == 0:
                        emotion = 1
                elif idx == 1:
                        emotion = 2
                elif idx == 2:
                        emotion = 0
                elif idx == 3:
                        emotion = 3


		# Only send if a person is detected - otherwise don't do anything
		if emotions[emotion]:
                	emotionData = emotions[emotion][0]
			for idxEmotionName, emotionName in enumerate(emotionNames):
				emotionString = " \"" + str(emotionNames[idxEmotionName]) + "/" + str(emotionData[idxEmotionName]) + "\""
				print("DEV: " + str(emotionString))
				os.system("mosquitto_pub -h localhost -t " + role + "/mime -m " + emotionString)

	print("Posted emotions for MQTT")
