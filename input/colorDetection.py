import time
from threading import Thread
from keys import *

# Import Azure API
from azure.cognitiveservices.vision.customvision.training import training_api
from azure.cognitiveservices.vision.customvision.training.models import ImageUrlCreateEntry
from azure.cognitiveservices.vision.customvision.prediction import prediction_endpoint
from azure.cognitiveservices.vision.customvision.prediction.prediction_endpoint import models

# Fetch the API keys
training_key = key("roleTraining")
prediction_key = key("rolePrediction")
project_key = key("roleProject")


# Get all info needed
trainer = training_api.TrainingApi(training_key)
project = trainer.get_project(project_key)
predictor = prediction_endpoint.PredictionEndpoint(prediction_key)

# Placeholder for colors
gotColors = ["", "", "", ""]

# Function for getting an unique timestamp for now
current_milli_time = lambda: int(round(time.time() * 1000))

# Get the color tag for a tile
def tileInfo(tileName, colorPosition):
	tile = open(tileName, mode="rb")
	results = predictor.predict_image(project.id, tile)
	tileColor = results.predictions[0].tag_name
	gotColors[int(colorPosition)] = tileColor
	return tileColor

# Main funtion that gives back the color tags
def colors():
	import os
	import image_slicer


	# Take image
	image_name = "single"
	os.system("sudo fswebcam -d /dev/video0 --no-banner -r 400x255 -S 10 " + image_name + ".jpg 2>/dev/null")


	# Slice image for needed area
	image_slicer.slice(image_name + ".jpg", 9)


	# Remove original images
	os.system("rm -rf " + image_name + ".jpg")


	# Remove unneeded image tiles
	os.system("rm -rf " + image_name + "_01_01.png;rm -rf " +image_name + "_01_03.png;rm -rf " + image_name + "_02_02.png;rm -rf " + image_name + "_03_01.png; rm -rf " + image_name + "_03_03.png")


	# Analyze the needed tiles
	tiles = ["_01_02", "_02_01", "_02_03", "_03_02"]


	# Placeholder for thread operations
	threads = []


	# Get unique timestamp for this round
	time = current_milli_time()


	# Loop through the files
	for idx, tile in enumerate(tiles):
		# Rename tile with millis for unique name
		os.rename(image_name + tile + ".png", image_name + tile + "_" + str(time) + ".png")
		tileName = image_name + tile + "_" + str(time) + ".png"

		# Start the threading for every tile
		threads.append(Thread(target=tileInfo, args=(tileName, idx,)))
		threads[idx].start()

		# Remove the analyzed tile
		os.system("rm -rf " + tileName)


	# Wait until all threads are done
	for idx, thread in enumerate(threads):
		threads[idx].join()


	# Output
	#for color in gotColors:
	#	print(color)
	return gotColors
