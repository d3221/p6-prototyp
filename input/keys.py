def key(type):
	# EDIT KEYS HERE IF NEEDED
	faceKey = "d72a4ca05e714928bd0d99cd335e3d3a"
	roleTrainingKey = "f2b39dfa6a214b79b11f9252b38d23cc"
	rolePredictionKey = "1a6b49839f0549ac829b7663a5a1e97f"
	roleProjectKey = "5964083e-d826-4a68-8941-2766c35fec9a"

	# Gives back the requested key
	if type == "face":
		return faceKey

	if type == "roleTraining":
		return roleTrainingKey

	if type == "rolePrediction":
		return rolePredictionKey

	if type == "roleProject":
		return roleProjectKey
