# TeamFlow (P6; SS 2018; IMD; Hochschule Darmstadt)

## Installation
  - Download the repo
  - Perform a ```npm install```
 
## Running /webview
 - Open the  ```index.html ``` in Chrome or Firefox
 - Switch to Fullscreen-Mode (Key F11)

## Running /input
 - Copy the repo on a RPi3
 - Navigate to /input
 - Execute ```python main.py```

## Running /speech
 - Go to ```/speech/browser/```
 - Open a Terminal window and execute ```node server.js```
 - Copy the URL in the Terminal window and paste it in your browser
 - Choose a seat position for this browser tab by changing the "Mircophone ID"
 - Hit "Start" and choose a microphone
 - Refreseh the /webview by pressing F5 key
 
## Restart
 - Cancel the python script on the RPi with CTRL+C/STRG+C
 - Send the following MQTT message: ```mosquitto_pub -h localhost -t "reset" -m "1"```
 - All systems are setting themselves to the default states
 - Start the python script again with ```python main.py```
 